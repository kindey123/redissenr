package com.kindey.redissen.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 作用描述
 *
 * @author Kindey.S [kindey123@163.com]
 * @version 1.0
 * @date 2023/9/6
 */
@Service
@Slf4j
public class TestService {
    @Autowired
    private RedisTemplate redisTemplate;

    @Scheduled(cron = "*/3 * * * * ?")
    public void getRedisData() {
        redisTemplate.opsForValue().set("bb","22");
        Object aa = redisTemplate.opsForValue().get("aa");
        log.info("aa:{}", null == aa ? null : aa.toString());
    }
}
