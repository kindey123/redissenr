package com.kindey.redissen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RedissenApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedissenApplication.class, args);
    }

}
